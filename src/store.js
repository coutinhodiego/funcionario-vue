import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex)
Vue.use(VueAxios, axios)


const client_username = 'jsxEBcnN-Ms_VLOWJrNhSw'
const client_password = 'qAOeyGIkmLJKtx9Y5MaUYFPPd5kiQD4hTI4GNDsg6FE'
const token = 'Basic ' + btoa(`${client_username}:${client_password}`)



export default new Vuex.Store({
  state: {
    token: localStorage.getItem('user-token') || '',
    // status: false,
    loading: false,
    user: '',
    avatars: []
  },


  mutations: {
    // authRequest (state, payload) {
    //   state.status = payload
    // },
    loading(state, payload) {
      state.loading = payload
    },
    authSuccess(state, token) {
      state.loading = true
      state.token = token
    },
    authError(state) {
      state.status = 'error'
    },
    setUser(state, payload) {
      state.user = payload
    },
    authLogout(state) {
      state.token = false
    },

    loadAvatars(state, payload) {
      state.avatars = payload
    }
  },


  actions: {
    authRequest({ commit, dispatch }, user) {
      return new Promise((resolve, reject) => { // The Promise used for router redirect in login
        commit('loading', true)

        axios.defaults.headers.common['Authorization'] = token;

        const config = new URLSearchParams();
        config.append('Content-Type', 'application/x-www-form-urlencoded')
        config.append('grant_type', 'password');
        config.append('username', user.email);
        config.append('password', user.password);

        axios.post('/v1/oauth2/token', config)
          .then(resp => {
            const accessToken = resp.data.access_token
            localStorage.setItem('user-token', accessToken) // store the token in localstorage
            commit('authSuccess', accessToken)
            // commit('loading', false)

            resolve(dispatch('logIn'))
          })
          .catch(err => {
            commit('authError', err)
            commit('loading', false)
            localStorage.removeItem('user-token') // if the request fails, remove any possible user token if possible
            reject(err)
          })
      })
    },

    logIn({ commit, state }) {
      return new Promise((resolve, reject) => {
        let accessToken = state.token

        let config = {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": `Bearer ${accessToken}`
          }
        }

        axios.get('/v1/employee/profile', config)
          .then(response => {
            let data = response.data
            commit('loading', false)
            let user = {
              _id: data._id,
              name: data.name,
              department: data.department,
              registration: data.registration,
              cellphone: data.cellphone,
              email: data.email,
              firebase: data.firebase,
              logs: data.logs,
              img: data.img
            }
            commit('setUser', user)
            resolve(user)
          })
          .catch(erro => {
            console.log(erro)
            commit('authError', erro)
            localStorage.removeItem('user-token') // if the request fails, remove any possible user token if possible
            reject(erro)
          })
      })
    },


    logOut({ commit }) {
      return new Promise((resolve, reject) => {
        commit('authLogout')
        localStorage.removeItem('user-token')
        // remove the axios default header
        delete axios.defaults.headers.common['Authorization']
        commit('setUser', '')
        resolve()
      })
    },

    registerUser({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit('loading', true)

        axios.defaults.headers.common['Authorization'] = token;

        const config = new URLSearchParams();
        config.append('Content-Type', 'application/x-www-form-urlencoded')
        config.append('name', user.name);
        config.append('email', user.email);
        config.append('password', user.password);
        config.append('department', user.department);
        config.append('registration', user.registration);
        config.append('cellphone', user.cellphone);
        config.append('img', user.img);

        axios.post('/v1/oauth2/register', config.toString())
          .then(response => {
            console.log(response)
            commit('loading', false)
            resolve(response)
          })
          .catch(erro => {
            console.log(erro)
            commit('authError', erro)
            reject(erro)
          })
      })
    },

    updateFirebase({ state }, payload) {

      if (payload.firebaseId) {
        let user = {
          email: payload.email,
          firebase: {
            id: payload.firebaseId
          }
        }
        // console.log(user)

        axios.put('/df/v1/firebase/update', user)
          .then(response => {
            console.log(response)
          })
          .catch(err => console.log(err))
      }
    },

    updateUser({ state, dispatch, commit }, payload) {
      commit('loading', true)
      return new Promise((resolve, reject) => {
        let user = payload
        let accessToken = state.token

        let headerConfig = {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": `Bearer ${accessToken}`
          }
        }

        const config = new URLSearchParams();
        config.append('name', user.name);
        config.append('email', user.email);
        config.append('department', user.department);
        config.append('registration', user.registration);
        config.append('cellphone', user.cellphone);
        config.append('img', user.img);

        console.log(user)
        axios.put('/v1/employee/profile', config.toString(), headerConfig)
          .then(() => {
            commit('loading', false)
            resolve(dispatch('logIn'))
          })
          .catch(err => {
            console.log(err)
            reject(err)
          })

      })
    },

    updatePassword({ state, dispatch, commit }, payload) {
      commit('loading', true)
      return new Promise((resolve, reject) => {
        let password = payload
        let accessToken = state.token

        let headerConfig = {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": `Bearer ${accessToken}`
          }
        }

        const config = new URLSearchParams();
        config.append('password', password.password);
        config.append('currentPassword', password.currentPassword);
        
        axios.put('/v1/employee/password', config.toString(), headerConfig)
          .then(() => {
            commit('loading', false)
            // resolve(dispatch('logOut'))
            resolve()
          })

      })
    },

    loadingAvatars({ commit }) {
      axios.get('/avatarRef.json')
        .then(response => {
          let data = response.data
          commit('loadAvatars', data)
        })
    }

  },


  getters: {
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status,
    getLoading: state => state.loading,
    logedUser: state => state.user,
    getAvatar: state => state.avatars
  }
})
