import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from './store'
import VueMaterial from 'vue-material'
import * as firebase from 'firebase'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'



Vue.use(VueAxios, axios)
Vue.use(VueMaterial)

Vue.config.productionTip = false
axios.defaults.baseURL = 'https://sofia-v3-dev.herokuapp.com';
// axios.defaults.baseURL = 'http://localhost:3000';

new Vue({
  router,
  store,
  render: function (h) { return h(App) },
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyBCfx9W3UIuZpXJbNG1v1MlrLvQMxRkwzY',
      authDomain: 'alerta-bot.firebaseapp.com',
      databaseURL: 'https://alerta-bot.firebaseio.com',
      projectId: 'alerta-bot',
      storageBucket: 'alerta-bot.appspot.com',
      messagingSenderId: '264724217634'
    })

    const messaging = firebase.messaging();
    messaging.usePublicVapidKey('BDgSgXQJp3hmLQ356yhIKeKKAkuyNfpbV9gppyf2BqSanQvWxhNux-ifHyF5b7Uc45SOhF3iUlFkzo7-5w5K1Sw');
    this.$store.dispatch('loadingAvatars')
  }
}).$mount('#app')
