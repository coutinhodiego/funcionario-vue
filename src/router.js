import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import UpdateAccount from './views/UpdateAccount.vue'
import store from './store'

Vue.use(Router)


const ifNotAuthenticated = (to, from, next) => {
  // console.log(store.getters.isAuthenticated)
  if (!store.getters.isAuthenticated) {
    next()
    return
  }
  next('/')
}

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next()
    return
  }
  next('/login')
}


export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      beforeEnter: ifAuthenticated,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      beforeEnter: ifNotAuthenticated
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: function () { 
        //   return import(/* webpackChunkName: "about" */ './views/About.vue')
        // }}
      },
      {
        path: '/register',
        name: 'register',
        component: Register,
        // beforeEnter: ifNotAuthenticated
      },
      {
        path: '/update-account',
        name: 'updateAccont',
        component: UpdateAccount,
        beforeEnter: ifAuthenticated
      },
  ]
})
