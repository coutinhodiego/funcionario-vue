importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');

const config = {
  apiKey: "AIzaSyBCfx9W3UIuZpXJbNG1v1MlrLvQMxRkwzY",
  authDomain: "alerta-bot.firebaseapp.com",
  databaseURL: "https://alerta-bot.firebaseio.com",
  projectId: "alerta-bot",
  storageBucket: "alerta-bot.appspot.com",
  messagingSenderId: "264724217634"
};

firebase.initializeApp(config);


const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
  console.log('setando')
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  console.log(payload)
  // Customize notification here
  var notificationTitle = 'Background Message Title';
  var notificationOptions = {
    body: 'Background Message body.',
    icon: '/firebase-logo.png'
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});